#
# Be sure to run `pod lib lint contact.me.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'contact.me'
  s.version          = '0.1.0'
  s.summary          = 'A short description of contact.me.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/grantmyles/contact.me/src'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Myles Grant' => 'pineconegrant@gmail.com' }
  s.source           = { :git => 'https://grantmyles@bitbucket.org/grantmyles/contact.me.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'Sources/*'

  s.vendored_libraries = "Libraries/libcontact.me.a"

  
  # s.resource_bundles = {
  #   'contact.me' => ['contact.me/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end


#{
#    "name": "GoogleAnalytics",
#    "version": "3.17.0",
#    "summary": "Google Analytics - measure your app performance",
#    "description": "Google Analytics lets you track application events you care about and gain insights from discovery and installation to conversion and engagement.",
#    "homepage": "https://www.google.com/analytics",
#    "license": {
#        "text": "Copyright 2016 Google",
#        "type": "Copyright"
#    },
#    "authors": "Google, Inc.",
#    "platforms": {
#        "ios": "5.0"
#    },
#    "source": {
#        "http": "https://www.gstatic.com/cpdc/5cd71dd2f756bb01/GoogleAnalytics-3.17.0.tar.gz"
#    },
#    "vendored_libraries": [
#    "Libraries/libGoogleAnalytics.a"
#    ],
#    "source_files": [
#    "Sources/GAI.h",
#    "Sources/GAIDictionaryBuilder.h",
#    "Sources/GAIEcommerceFields.h",
#    "Sources/GAIEcommerceProduct.h",
#    "Sources/GAIEcommerceProductAction.h",
#    "Sources/GAIEcommercePromotion.h",
#    "Sources/GAIFields.h",
#    "Sources/GAILogger.h",
#    "Sources/GAITrackedViewController.h",
#    "Sources/GAITracker.h"
#    ],
#    "frameworks": [
#    "CoreData",
#    "SystemConfiguration"
#    ],
#    "libraries": [
#    "z",
#    "sqlite3"
#    ]
#}

